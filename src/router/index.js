import { createRouter, createWebHashHistory } from 'vue-router';


import About from '@/views/About.vue';
import Contact from '@/components/ContactPage.vue';
import Login from '@/views/LoginPage.vue';
import CityList from '@/views/CityList.vue';
import WeatherComponent from '@/components/WeatherPage.vue';

const routes = [
    { path: '/', component: WeatherComponent, name: 'home' },
    { path: '/about', component: About },
    { path: '/contact', component: Contact },
    { path: '/login', component: Login, name: 'login' },
    { path: '/cities', component: CityList, name: 'cities' },
];

const router = createRouter({
    history: createWebHashHistory(),
    routes,
});


export default router
